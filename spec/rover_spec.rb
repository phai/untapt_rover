require 'spec_helper'

describe Rover do
  let (:plateau) { ['5', '5'] }
  it { expect(described_class.new(plateau, ['1', '2', 'N']).process('LMLMLMLMM')).to eq("1 3 N") }
  it { expect(described_class.new(plateau, ['3', '3', 'E']).process('MMRMMRMRRM')).to eq("5 1 E") }
  it { expect(described_class.new(plateau, ['3', '3', 'E']).process('MMRMMRMRRMMM')).to eq("5 1 E") }
  it { expect(described_class.new(plateau, ['3', '3', 'E']).process('RRRL')).to eq("3 3 W") }
  it { expect(described_class.new(plateau, ['0', '0', 'W']).process('M')).to eq("0 0 W") }
end
