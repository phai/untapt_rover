class Rover

  class UnknownAction < StandardError; end
  class UnknownDirection < StandardError; end

  module Action
    SPIN_LEFT  = 'L'.freeze
    SPIN_RIGHT = 'R'.freeze
    MOVE       = 'M'.freeze
  end

  module Direction
    NORTH = 'N'.freeze
    EAST  = 'E'.freeze
    SOUTH = 'S'.freeze
    WEST  = 'W'.freeze
  end

  #
  # rover: [x, y, face]
  # plateau, [x, y]
  #
  def initialize(plateau, rover)
    @rover   = { x: rover[0].to_i, y: rover[1].to_i, face: rover[2] }
    @plateau = { x: plateau[0].to_i, y: plateau[1].to_i }
  end

  def process(actions)
    actions.each_char do |action|
      case action
      when Action::SPIN_LEFT
          spin_left
      when Action::SPIN_RIGHT
          spin_right
      when Action::MOVE
          move
      else
        raise UnknownAction
      end
    end
    to_string
  end

  def to_string
    "#{@rover[:x]} #{@rover[:y]} #{@rover[:face]}"
  end

  private

  def spin_left
    case @rover[:face]
    when Direction::NORTH
      @rover[:face] = Direction::WEST
    when Direction::EAST
      @rover[:face] = Direction::NORTH
    when Direction::SOUTH
      @rover[:face] = Direction::EAST
    when Direction::WEST
      @rover[:face] = Direction::SOUTH
    else
      raise UnknownDirection
    end
  end

  def spin_right
    case @rover[:face]
    when Direction::NORTH
      @rover[:face] = Direction::EAST
    when Direction::EAST
      @rover[:face] = Direction::SOUTH
    when Direction::SOUTH
      @rover[:face] = Direction::WEST
    when Direction::WEST
      @rover[:face] = Direction::NORTH
    else
      raise UnknownDirection
    end
  end

  def move(count = 1)
    case @rover[:face]
    when Direction::NORTH
      @rover[:y] = [ @rover[:y] + count, @plateau[:y] ].min
    when Direction::EAST
      @rover[:x] = [ @rover[:x] + count, @plateau[:x] ].min
    when Direction::SOUTH
      @rover[:y] = [ @rover[:y] - count, 0 ].max
    when Direction::WEST
      @rover[:x] = [ @rover[:x] - count, 0 ].max
    else
      raise UnknownDirection
    end
  end
end
