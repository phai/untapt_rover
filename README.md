# Untapt Rover

## Prerequisites
```
# install ruby
# then
bundle install
```

## HowTo run
```
bundle exec ruby test.rb <file>
e.g:
bundle exec ruby test.rb test.md
```

## HowTo run test
```
bundle exec rspec
```

