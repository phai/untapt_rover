require_relative 'rover'

plateau = gets.split(" ")
while input = gets
  rover = input.split(" ")
  actions = gets
  puts Rover.new(plateau, rover).process(actions.split(" ").first)
end
